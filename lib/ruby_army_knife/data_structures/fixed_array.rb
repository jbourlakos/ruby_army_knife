module RubyArmyKnife

  module DataStructures

  class FixedArray < ::Array


    def initialize(size=0,&block)
      super(size,&block)
    end



    def <<(obj)
      ([] + self) << obj
    end


    def []=(start, length=1, value)
      if start.kind_of? Range
        range = start
        unless self.include_index? range.min
          raise IndexError.new("index #{range.min} too small for array; minimum: #{-self.length}")
        end
        unless self.include_index? range.max
          raise IndexError.new("index #{range.max} too large for array; maximum: #{self.length-1}")
        end
        return super
      end
      super(start,length,value)
    end


    def clear
      []
    end



    def collect!(&block)
      collect(&block)
    end



    def compact!
      compact
    end



    def concat(other_ary)
      []+self+other_ary
    end


    def delete(obj,&block)
      ([]+self).delete(obj,&block)
    end


    def delete_at(index)
      ([]+self).delete_at(index)
    end


    def delete_if(&block)
      ([]+self).delete_if(&block)
    end


    def drop(n)
      ([]+self).drop(n)
    end


    def drop_while(&block)
      ([]+self).drop_while(&block)
    end


    # TODO: def fill


    def flatten!(level=-1)
      ([]+self).flatten!(level)
    end


    # TODO: def insert


    def keep_if(&block)
      ([] + self).keep_if(&block)
    end



    def map!(&block)
      map(&block)
    end



    def pop(n=1)
      ([]+self).pop(n)
    end



    def push(obj,*more)
      ([]+self).push(obj,*more)
    end


    # TODO: def reject!



    # TODO: def replace



    def reverse!
      reverse
    end


    def rotate!(count=1)
      rotate(count)
    end


    def select!(&block)
      select(block)
    end


    # TODO:shift


    def shuffle!(*args)
      shuffle(*args)
    end



    # TODO: slice


    



    def include_index? index
      index >= -self.length && index < self.length
    end


  end

end
end