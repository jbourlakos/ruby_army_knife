module RubyArmyKnife 

  module DataStructures

  class CharMatrix

    def initialize(height, width, default_character="\0", transparency_character="\0", &char_init_block)
      raise ArgumentError.new("Invalid width: #{width}") unless width.respond_to?(:to_i)
      raise ArgumentError.new("Width must be larger than zero. Current value: #{width}") unless width.to_i > 0
      @width = width.to_i
      raise ArgumentError.new("Invalid height: #{height}") unless height.respond_to?(:to_i)
      raise ArgumentError.new("Height must be larger than zero. Current value: #{height}") unless height.to_i > 0
      @height = height.to_i
      raise ArgumentError.new("Invalid default character: '#{default_character}'") unless default_character.respond_to?(:to_s) && default_character.to_s.length > 0
      @default_character = default_character.to_s[0]
      raise ArgumentError.new("Invalid transparency character: '#{transparency_character}'") unless transparency_character.respond_to?(:to_s) && transparency_character.to_s.length > 0
      @transparency_character = transparency_character.to_s[0]
      @matrix = []
      (0...height).each do |r|
        @matrix[r] = []
        (0...width).each do |c|
          @matrix[r][c] = block_given? ? yield(r,c,@height,@width) : default_character
        end
      end
    end


    attr_reader :width, :height, :default_character, :transparency_character


    def [](i)
      @matrix[i]
    end


    def overlay(char_matrix, row=0, col=0, retain_transparencies=false)
      w1 = self.width
      h1 = self.height
      w2 = char_matrix.width
      h2 = char_matrix.height
      top = row < 0 ? row : 0
      left = col < 0 ? col : 0
      right = (w2 + col) > w1 ? (w2 + col) : w1
      bottom = (h2 + row) > h1 ? (h2 + row) : h1
      coff1 = col < 0 ? -col : 0
      roff1 = row < 0 ? -row : 0
      coff2 = col < 0 ? 0 : col
      roff2 = row < 0 ? 0 : row
      new_height = bottom - top
      new_width = right - left
      result = CharMatrix.new(new_height, new_width)
      (0...self.height).each do |r|
        (0...self.width).each do |c|
          result[r+roff1][c+coff1] = self[r][c] if self[r][c] != self.transparency_character || retain_transparencies
        end
      end
      (0...char_matrix.height).each do |r|
        (0...char_matrix.width).each do |c|
          result[r+roff2][c+coff2] = char_matrix[r][c] if char_matrix[r][c] != char_matrix.transparency_character || retain_transparencies
        end
      end
      result
    end


    def border(decoration=' ', width=1)
      CharMatrix
        .new(self.height + width*2, self.width + width*2, decoration)
        .overlay(self, width, width, true)
    end


    def clone
      CharMatrix.new(self.height,self.width) do |r,c|
        self[r][c]
      end
    end


    def draw_horizontal(str, row, col)
      str_matrix = CharMatrix.new(1, str.length) {|r,c| str[c] }
      self.overlay str_matrix, row, col
    end


    def draw_vertical(str, row, col)
      str_matrix = CharMatrix.new(str.length,1) {|r,c| str[r] }
      self.overlay str_matrix, row, col
    end


    def row(r)
      @matrix[r]
    end


    def col(c)
      @matrix.map{|r| r[c] }
    end


    def crop(r1=0, c1=0, r2=@height-1, c2=@width-1)
      CharMatrix.new(r2-r1+1, c2-c1+1) do |r,c,h,w|
        @matrix[r+r1][c+c1]
      end
    end


    def to_s(transparency_fill=' ')
      @matrix
        .map { |row| row.join }
        .map{ |str| str.gsub(/#{self.transparency_character}/, transparency_fill) }
        .join("\n")
    end

  end

end

end