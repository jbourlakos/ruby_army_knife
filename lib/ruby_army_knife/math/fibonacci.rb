require_relative "./functions.rb"

module RubyArmyKnife


    module Math

        class Fibonacci

            include ::Math
            include ::Enumerable
            include Functions

            def initialize operation, *base_terms
                operation_compatible = base_terms.map {|t| t.respond_to? operation }.reduce(:&)
                raise ArgumentError.new("Not all base terms respond to #{operation}") unless operation_compatible
                @base_terms = base_terms
                @operation = operation.to_s.to_sym
            end


            attr_reader :operation


            def base_terms
                @base_terms.dup
            end


            def each(&block)
                 (0..Float::INFINITY).lazy.map{ |i| self[i] }.each(&block)
            end


            def order
                @base_terms.length
            end


            def [](idx)
                if idx.kind_of? Integer
                    get_fixnum idx
                elsif idx.kind_of? Range
                    get_range idx
                else
                    raise ArgumentError.new("Invalid index #{idx}")
                end
            end



            def +(fib)
                raise ArgumentError.new ("Cannot add Fibonacci series of different operation: #{self.operation} and #{fib.operation}") if (fib.operation != self.operation)
                raise ArgumentError.new ("Cannot add Fibonacci series of different order: #{self.order} and #{fib.order}") if (fib.order != self.order)
                new_base_terms = self.base_terms.zip(fib.base_terms).map {|t1,t2| t1+t2}
                Fibonacci.new(self.operation, *new_base_terms)
            end



            def magnitude(idx)
                value = self[idx]
                if value.respond_to? :map
                    return value.map {|v| ceil(log10(v)) }
                end
                ceil(log10(value))
            end



            private

            def get_fixnum(idx)
                raise ArgumentError.new("Invalid index #{idx}") unless idx.kind_of?(Integer) && idx >= 0
                return @base_terms[idx] if idx < order 
                queue = []
                @base_terms.reverse.each { |t| queue << t }
                (order..idx).each do |i|
                    term = queue.reduce(self.operation)
                    queue.shift
                    queue << term
                end
                queue.last
            end


            def get_range(range)
                raise ArgumentError.new("Invalid range #{range}") unless range.kind_of?(Range)
                range = (range.min.to_i..range.max.to_i)
                base_queue = get_base_range(range)
                min = range.size > self.order ? 0 : range.size-self.order
                while min < range.min
                    base_queue << base_queue.last(self.order).reduce(self.operation)
                    base_queue.shift
                    min += 1
                end
                base_queue
            end


            def get_base_range(range)
                raise ArgumentError.new("Invalid range #{range}") unless range.kind_of?(Range)
                result = @base_terms.dup
                if range.size <= self.order
                  return result
                end
                min = 0
                max = range.size-1
                (range.size - self.order).times do 
                    result << result[min..max].reduce(self.operation)
                    min+=1
                    max+=1
                end
                result
            end


        end


    end

end