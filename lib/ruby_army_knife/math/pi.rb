require 'ruby_army_knife/data_structures/char_matrix.rb'

module RubyArmyKnife

  module Math


    class Pi

      def self.by_random_numbers(amount, limit=1000)
        coprime = 0
        cofactor = 0
        amount.times do 
          if rand(limit).gcd(rand(limit)) == 1
            coprime += 1
          else
            cofactor +=1
          end
        end
        ::Math.sqrt(6 / (coprime.to_f / amount))
      end



      def self.by_random_cartesian_matrix(size, throws)
        empty_char = '.'
        circle_char = "'"
        hit_char = '#'
        matrix = CartesianCharMatrix.new(size,empty_char)
        throws.times do
          random_row = rand(size)
          random_col = rand(size)
          matrix[random_row][random_col] = hit_char
        end
        radius = size/2
        inside_circle = 0
        outside_circle = 0
        (0...size).each do |r|
          (0...size).each do |c|
            x = matrix.translate_row_to_x(r)
            y = matrix.translate_col_to_y(c)
            if x**2 + y**2 <= radius**2 && matrix[r][c] == empty_char
              matrix[r][c] = circle_char
            end
            if matrix[r][c] == hit_char && x**2 + y**2 <= radius**2
              inside_circle += 1
            elsif matrix[r][c] == hit_char
              outside_circle += 1
            else
              next
            end
          end
        end
        4 * (inside_circle.to_f / (inside_circle+outside_circle))
      end



      class CartesianCharMatrix < ::RubyArmyKnife::DataStructures::CharMatrix

        def initialize(size,default_character)
          super(size,size,default_character)
          @x0 = width / 2
          @y0 = height / 2
        end

        def get(x,y)
          col = @x0 + x
          row = @y0 + y
          self[row][col]
        end

        def set(x,y,value)
          col = @x0 + x
          row = @y0 + y
          self[row][col] = value
        end

        def translate_row_to_x(row)
          row - @x0
        end

        def translate_col_to_y(col)
          col - @y0
        end

      end


    end

  end

end