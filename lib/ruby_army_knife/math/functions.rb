module RubyArmyKnife

  module Math


    module Functions

      def ceil(n)
        return n if n.kind_of? Integer
        n.to_i + 1
      end

      def floor(n)
        return n if n.kind_of? Integer
        n.to_i
      end

    end

  end
end